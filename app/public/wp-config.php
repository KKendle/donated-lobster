<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
if (file_exists(dirname(__FILE__) . '/local.php')) {
	// dev settings
	define( 'DB_NAME', 'local' );
	define( 'DB_USER', 'root' );
	define( 'DB_PASSWORD', 'root' );
	define( 'DB_HOST', 'localhost' );
}
else {
	// prod settings
}

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'H9MNV3r3WIvYGWUq3Kk+SD4haSStTio4aPZo61juogZfFf5vfXLZxlfZOZCT2DKq9MPAGBZP+EduGvf6A0w+OA==');
define('SECURE_AUTH_KEY',  'hVe5EjSVMWkGqwh4ByJ8u9gyFet0p6IuDZcQbPOvPXJ6LHzybjvx6Hgmgs2MLep7P+wwr6qUK3JO87JZCwrh9g==');
define('LOGGED_IN_KEY',    'lPwNY1ZYj+x7f2ZlJrGPkM/6aly/pGNBXvuW7ihUOqw7ugXe5a7V0UuwU6W/9ejvyHshjS/iVeNavyi4rG8xaQ==');
define('NONCE_KEY',        'Jl2MQCpNmwxYsbH6u6EtBpERtL5lvWQgqYfOuSYsb6LVPztkNOPBCANrnsrk2523rlFkJ5DLGCUQnXFJO5AjMg==');
define('AUTH_SALT',        'DUJy9BAQglzyorXh50mxz7O6j8iGEHWf9gAfmu7VvUoO3hrmlaNd7q+v7scVLxDR4Cr92KFLHT/qiF0lwiEOpg==');
define('SECURE_AUTH_SALT', 'r1guGI19msr2QAbr/aOBYFigYeT0N3M21HP0T2U3JCeSglFYfJDEDzlmKsYX2ft9aOKuA/2n8MYb0aJ71Ir7EQ==');
define('LOGGED_IN_SALT',   'mTkxSsJfsxeuMH346OvTUrPGeDo3JkkCqUOFe5xJT0/v1kpieELrwRGZHBT133V92e/hJpRaI8TRiWNrrWeaLQ==');
define('NONCE_SALT',       'cNn9kVB0o51xPBXYS2jz/3xjo2hcNX3GTnR+Xps0yKU/qu3xJVGm00d47NKxzKq0M1OmoUPI8Im61tUd3pVWkw==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
